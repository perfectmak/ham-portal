'use strict';

/**
 * Module and Controller class for external authentication
 * This page is mean't to handle external authentication.
 * For now, it should only be used by messenger.
 * Authentication is handled by Auth0
 *
 * @author Perfect Makanju<perfect@kudi.ai>.
 */
(function() {
  angular.module('Auth', [
    'BlurAdmin.pages.services'
  ])
  // .controller('AuthController', AuthController)
  .run(['$rootScope', 'Auth', init]);

  /**
  * Initialize the Authentication.
  */
  function init($rootScope, AuthService) {
    if(!AuthService.isAuthenticated()) {
      AuthService.authenticate();
    } else {
      redirectWithUser(true, AuthService.getAuthenticatedUser());
    }

    /**
     * Handle user authentication redirects
     */
    AuthService.onAuthenticated(function (err, profile) {
      if(err) {
        redirectWithUser(false)
      } else {
        redirectWithUser(true, profile);
      }
    });
  }

  /**
   * The all too popular get parameter url javascript
   *
   * @param name
   * @param url
   * @returns {*}
   */
  function getParameterByName(name, url) {
    if (!url) {
      url = window.location.href;
    }
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
      results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
  }

  /**
   * Redirects with users profile info.
   *
   * @papram success
   * @param user facebook user data
   */
  function redirectWithUser(success, user) {
    var redirectUrl = getParameterByName('redirect_uri');
    if (success) {
      document.querySelector('#success-msg').classList.remove('hidden');
      setTimeout(function(){
        if(redirectUrl) {
          location.href = redirectUrl + "&authorization_code=" + user.user_id;
        }
      }, 500);

    } else {
      document.querySelector('#error-msg').classList.remove('hidden');
      setTimeout(function(){
        if(redirectUrl) {
          location.href = redirectUrl;
        }
      });

    }
  }

})();