/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('BlurAdmin.pages.appliances', [])
      .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
        .state('appliances', {
          url: '/appliances',
          templateUrl: 'app/pages/appliances/appliances.html',
          title: 'Appliances',
          sidebarMeta: {
            icon: 'ion-monitor',
            order: 1,
          },
          controller: 'AppliancesCtrl'
        });
  }

})();
