/**
 * @author v.lugovksy
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('BlurAdmin.pages.dashboard')
      .controller('AppliancesCtrl', ['$scope', 'Auth', 'Appliance', AppliancesCtrl]);

  /** @ngInject */
  function AppliancesCtrl($scope, AuthService, ApplianceService) {
    if(AuthService.isAuthenticated()) {
      ApplianceService
        .fetchAll()
        .then(function (appliances) {
          if(appliances) {

          }
          console.log('Appliances gotten', appliances);
        });
    }
  }
})();