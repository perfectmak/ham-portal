/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('BlurAdmin.pages', [

    'BlurAdmin.pages.services',
    'BlurAdmin.pages.dashboard',
    'BlurAdmin.pages.appliances',
    'BlurAdmin.pages.ui',
    'BlurAdmin.pages.components',
    'BlurAdmin.pages.form',
    'BlurAdmin.pages.tables',
    'BlurAdmin.pages.charts',
    'BlurAdmin.pages.maps',
    'BlurAdmin.pages.profile',
  ])
  .config(routeConfig)
  .constant('Config', {
    // baseUrl: 'http://home-appliance-manager.herokuapp.com',
    baseUrl: 'http://localhost:1337'
  })
  .run(init);

  /** @ngInject */
  function routeConfig($urlRouterProvider, baSidebarServiceProvider) {
    $urlRouterProvider.otherwise('/dashboard');

    baSidebarServiceProvider.addStaticItem({
      title: 'Pages',
      icon: 'ion-document',
      subMenu: [{
        title: 'Sign In',
        fixedHref: 'auth.html',
        blank: true
      }, {
        title: 'Sign Up',
        fixedHref: 'reg.html',
        blank: true
      }, {
        title: 'User Profile',
        stateRef: 'profile'
      }, {
        title: '404 Page',
        fixedHref: '404.html',
        blank: true
      }]
    });
  }

  /** @ngInject **/
  function init($rootScope, $http, Auth, Config) {
    if(!Auth.isAuthenticated()) {
      Auth.authenticate();
    }

      /**
       * Handle user authentication redirects
       */
    Auth.onAuthenticated(function (err, profile) {
      if(err) {
        alert('An error occurred with authentication. Refresh the page and try re-authenticating.');
        console.log('Authentication error', err);
      }
      else {
        $http.post(Config.baseUrl + '/auth/fb', { email: profile.email, facebook_id: profile.user_id })
          .then(function(response) {
            if(response.data.status === 'success') {
              Auth.saveBotUser(response.data.data);
            } else {
              console.log(response.data);
            }
          })
          .catch(function(err) {
            alert('An error occurred with authentication. Refresh the page and try re-authenticating.');
            console.log(err);
          });
      }
    });
  }

})();
