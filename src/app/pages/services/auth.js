'use strict';

/**
 * Handles User Authentication
 *
 * @author Perfect Makanju<perfect@kudi.ai>.
 */
(function () {
    'use strict';

    angular.module('BlurAdmin.pages.services', [
      'ui.router',
      'auth0.lock',
      'auth0.auth0'
    ])
    .config(auth0Config)
    .service('Auth', AuthService);


  function auth0Config(lockProvider, angularAuth0Provider) {
    lockProvider.init({
      clientID: 'qXN94GbaqqK16sJrMlPseYsEnqPmlG6i',
      domain: 'postalytics.auth0.com',
      options: {
        allowedConnections: ['facebook'], //allow only facebook login,
        closable: false //prevent closing of the auth window
      }
    });

    angularAuth0Provider.init({
      clientID: 'qXN94GbaqqK16sJrMlPseYsEnqPmlG6i',
      domain: 'postalytics.auth0.com'
    });
  }

    /**
     *
     * @param lock Auth0 Lock
     * @param angularAuth0 Auth0 Libaray
     */
    function AuthService(lock, angularAuth0) {
        var profileKey = 'profile';
        var tokenKey = 'token';
        var botUserKey = 'botUser';

        this.authenticate = function() {
            lock.show();
        };

        this.isAuthenticated = function() {
            return !!localStorage.getItem(profileKey);
        };

        /**
         *  Helper for handling authentication redirects
         *
         * @param callback
         */
        this.onAuthenticated = function(callback) {
            // For use with UI Router
            lock.interceptHash();

            lock.on('authenticated', function(authResult) {
                lock.hide();

                localStorage.setItem('id_token', authResult.idToken);
                lock.getProfile(authResult.idToken, function(error, profile) {
                    if (error) {
                        callback(error);
                    }
                    callback(null, profile);

                    localStorage.setItem(profileKey, JSON.stringify(profile));
                });
            });
        };

        this.getAuthenticatedUser = function() {
            return JSON.parse(localStorage.getItem(profileKey));
        };

        this.signout = function(redirectUrl) {
            localStorage.removeItem(profileKey);
            localStorage.removeItem(tokenKey);
            angularAuth0.logout({returnTo: redirectUrl}, {version: 'v2'});
        };

        this.getUserToken = function() {
          return this.getBotUser().token;
        };

        this.saveBotUser = function(user) {
          localStorage.setItem(botUserKey, JSON.stringify(user));
        };

        this.getBotUser = function() {
          return JSON.parse(localStorage.getItem(botUserKey));
        };
    }
})();