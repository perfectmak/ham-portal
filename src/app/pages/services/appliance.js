'use strict';

/**
 * Handle Appliance Loading and Update
 *
 * @author Perfect Makanju<perfect@kudi.ai>.
 */
(function () {
  'use strict';

  angular.module('BlurAdmin.pages')
    .service('Appliance', ['$http', 'Auth', 'Config', ApplianceService]);


  /**
   *
   * @param $http Http
   */
  function ApplianceService($http, Auth, Config) {
    this.http = $http;
    this.Auth = Auth;
    this.currentUser = Auth.getAuthenticatedUser();
    this.Config = Config;
  }

  ApplianceService.prototype =  {
    url: function(path) {
      return this.Config.baseUrl + path;
    },

    fetchAll: function() {
      console.log(this.currentUser);
      return this.http.get(this.url('/users/' + this.currentUser.id + '/appliances'))
        .then(function success(response) {
          return response.data;
        })
        .catch(function error(errResponse) {
          //error
          console.log(errResponse);
        });
    }
  };

})();