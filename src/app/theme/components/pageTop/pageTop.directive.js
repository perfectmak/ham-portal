/**
 * @author v.lugovksy
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('BlurAdmin.theme.components')
      .directive('pageTop', pageTop);

  /** @ngInject */
  function pageTop(Auth) {
    return {
      restrict: 'E',
      templateUrl: 'app/theme/components/pageTop/pageTop.html',
      link: function($scope, elem) {
        if(Auth.isAuthenticated()) {
          var authUser = Auth.getAuthenticatedUser();

          $scope.profilePicture = authUser.picture;

          $scope.logout = function() {
              Auth.signout('http://localhost:3000');
          };
        }
      }
    };
  }

})();