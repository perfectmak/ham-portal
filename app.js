'use strict';

/**
 *
 * @author Perfect Makanju<perfect@kudi.ai>.
 */
const path = require('path');
const express = require('express');
const bodyParser = require('body-parser');
const request = require('request');
const app = express();
const port = process.env.PORT || 3000;

// Process application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({extended: false}));

// Process application/json
app.use(bodyParser.json());

//load static files from views
app.use(express.static(path.join(__dirname + '/release')));


app.listen(port, () => {
  console.log(`Ham Portal is running on port ${port}`);
});